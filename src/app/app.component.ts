import { Component } from '@angular/core';

import {MatDialog, MatDialogConfig} from "@angular/material";

import { Color, Tile, TileSet, GroupTileSet, RunTileSet, Gameboard, GameboardInterface, Hand } from './lib/models';
import { TileSelectionComponent } from './tile-selection/tile-selection.component';
import { TileSetSelectionComponent } from './tile-set-selection/tile-set-selection.component';
import { HandSelectionComponent } from './hand-selection/hand-selection.component';
import { SolutionsComponent } from './solutions/solutions.component';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  board: GameboardInterface;
  hand: Hand;

  constructor(private dialog: MatDialog){
    this.hand = new Hand();
    this.board = new Gameboard();

    let colors = [Color.red, Color.black, Color.blue, Color.green];
    for(let color of colors){
      let tileSet = new RunTileSet(color);
      for(let i = 1; i <= 3; i++){
        tileSet.add(new Tile(i, color));
      }

      this.board.addTileSet(tileSet);
    }
  }

  onAddSetClicked(){
    const dialogRef = this.dialog.open(TileSetSelectionComponent, {
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != null){
        this.board.addTileSet(result);
      }
    });  
  }

  onEditHandClicked(){
    const dialogRef = this.dialog.open(HandSelectionComponent, {
      data: this.hand
    });
  }

  onEditSetClicked(tileSet: TileSet){
    const dialogRef = this.dialog.open(TileSelectionComponent, {
      data: tileSet
    });
  }

  onHelpClicked(){
    let allTiles = [];

    for(let tile of this.hand.tiles.tiles()){
      allTiles.push(tile);
    }

    for(let set of this.board.getTileSets()){
      for(let tile of set.tiles()){
        allTiles.push(tile)
      }
    }

    let validSets = this.board.getValidSets(allTiles);

    const dialogRef = this.dialog.open(SolutionsComponent, {
      data: validSets
    });
    }
}
