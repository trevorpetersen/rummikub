import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';

import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule, MatIconModule} from "@angular/material";
import {MatSelectModule} from '@angular/material/select';
import {MatTabsModule} from '@angular/material/tabs';
import {MatInputModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';

import { TileSelectionComponent } from './tile-selection/tile-selection.component';
import { TileSetSelectionComponent } from './tile-set-selection/tile-set-selection.component';
import { TileSetComponent } from './tile-set/tile-set.component';
import { TileComponent } from './tile/tile.component';
import { HandSelectionComponent } from './hand-selection/hand-selection.component';
import { SolutionsComponent } from './solutions/solutions.component';



@NgModule({
  declarations: [
    AppComponent,
    TileSelectionComponent,
    TileSetSelectionComponent,
    TileSetComponent,
    TileComponent,
    HandSelectionComponent,
    SolutionsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatIconModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatTabsModule,
    MatCardModule
  ],
  providers: [],
  entryComponents: [
    TileSelectionComponent,
    TileSetSelectionComponent,
    HandSelectionComponent,
    SolutionsComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
