import { Component, OnInit, Inject } from '@angular/core';
import { TileSelectionComponent } from '../tile-selection/tile-selection.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Hand, Tile, Gameboard } from '../lib/models';

@Component({
  selector: 'app-hand-selection',
  templateUrl: './hand-selection.component.html',
  styleUrls: ['./hand-selection.component.scss'],
})
export class HandSelectionComponent implements OnInit {

  public Gameboard = Gameboard; 

  constructor(
  public dialogRef: MatDialogRef<TileSelectionComponent>,
  @Inject(MAT_DIALOG_DATA) public data: Hand) {}
  
  ngOnInit() {
    console.log(this.data);
  }

  public numberInHand(tile: Tile){
    if(tile.isWild()){
      return this.data.tiles.tiles().filter(t => t.isWild()).length;
    }else{
      return this.data.tiles.tiles().filter(t => t.equals(tile)).length;
    } 
  }

  public getTileClass(tile: Tile){
    return {
      'see-through': this.numberInHand(tile) == 0
    }
  }

}
