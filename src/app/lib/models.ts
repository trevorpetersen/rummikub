import { StringBuilder } from 'typescript-string-operations';

const NUM_COLORS: number = 4;
const NUM_NUMBERS: number = 13;
export enum Color{
    black, blue, green, red
}

export class Tile{
    value: number;
    color : Color;

    constructor(value: number, color: Color){
        this.value = value;
        this.color = color;
    }

    public isWild():boolean{return this.value == null || this.value < 0;}

    public clone(): Tile{
        return new Tile(this.value, this.color);
    }

    public toString(): String{
        return `${this.color} ${this.value}`
    }
    public equals(tile: Tile) : boolean{
        return tile != null && tile.value == this.value && tile.color == this.color;
    }

    public static compare(tile1: Tile, tile2: Tile): number{
        if(tile1.isWild() || tile2.isWild()) return tile1.value - tile2.value;


        if(tile1.color == tile2.color) return tile1.value - tile2.value;
        return tile1.color - tile2.color;
    }
}

export abstract class TileSet{
    static readonly MIN_SET_LENGTH = 3;

    protected _tiles: Tile[];

    constructor(){
        this._tiles = [];
    }

    abstract canAdd(tile: Tile): boolean;
    abstract isValid(): boolean;
    abstract clone(): TileSet;
    
    public tiles() : Array<Tile>{
        return this._tiles;
    }
    public length() : number{
        return this._tiles.length;
    }
    public containsTile(tile: Tile): boolean {
        return this._tiles.filter(t => tile.equals(t)).length > 0;
    }
    public add(tile: Tile): boolean {
        if(! this.canAdd(tile)) return false;

        this._tiles.push(tile);
        this._tiles = this._tiles.sort(Tile.compare);
        return true;
    }
    public addAll(tiles: Array<Tile>): boolean{
        return tiles
            .map(t => this.add(t))
            .reduce((t1, t2) => t1 && t2, true);
    }
    public remove(tile: Tile): boolean{
        for(let i = 0; i < this._tiles.length; i++){
            if(this._tiles[i].equals(tile)){
                this._tiles.splice(i, 1);
                return true;
            }
        }
        return false;
    }

    public toString(): String{
        return this._tiles.map(t => t.toString()).toString();
    }

    public equals(tileSet: TileSet): boolean{
        if(this.length != tileSet.length) return false;

        let contains1 = this.tiles()
            .map(t => tileSet.containsTile(t))
            .reduce((a,b) => a && b, true );

        let contains2 = tileSet.tiles()
            .map(t => this.containsTile(t))
            .reduce((a,b) => a && b, true );

        return contains1 && contains2;
    }
}

export class GroupTileSet extends TileSet{

    private value: number;
    constructor(value: number){
        super();
        this.value = value;
    }

    canAdd(tile: Tile): boolean {
        if(tile.isWild()) return true;
        
        return tile.value == this.value && !this.containsTile(tile);
    }

    isValid(): boolean{
        return this._tiles.length >= TileSet.MIN_SET_LENGTH;
    }

    clone(){
        let copy: GroupTileSet = new GroupTileSet(this.value);
        copy.addAll(this._tiles.map(t => t.clone()));

        return copy;
    }

    public toString(): String{
        return `G${super.toString()}`;
    }
}

export class RunTileSet extends TileSet{

    private color: Color;
    constructor(color: Color){
        super();
        this.color = color;
    }

    canAdd(tile: Tile): boolean {
        if(tile.isWild()) return true;

        return tile.color == this.color && ! this.containsTile(tile);
    }    

    isValid(): boolean{
        if (this._tiles.length < TileSet.MIN_SET_LENGTH) return false;

        let numWilds: number = this._tiles.filter(t => t.isWild()).length;

        return this.numGaps() <= numWilds;
    }

    private numGaps(): number{
        let gaps: number = 0;
        let nonWilds: Array<Tile> = this._tiles.filter(t => ! t.isWild());
        nonWilds.sort((t1, t2) => t1.value - t2.value);

        for(let i = 0; i < nonWilds.length - 1; i++){
            gaps += (nonWilds[i+1].value - nonWilds[i].value) - 1;
        }

        return gaps;
    }

    
    clone(){
        let copy: RunTileSet = new RunTileSet(this.color);
        copy.addAll(this._tiles.map(t => t.clone()));

        return copy;
    }

    public toString(): String{
        return `R${super.toString()}`;
    }
}

export class AnyTileSet extends TileSet{
    constructor(){
        super();
    }

    canAdd(tile: Tile): boolean {
        return true;
    }

    isValid(): boolean{
        return true;
    }

    clone(){
        let copy: AnyTileSet = new AnyTileSet();
        copy.addAll(this._tiles.map(t => t.clone()));

        return copy;
    }
}


export abstract class GameboardInterface{
    abstract getTileSets(): Array<TileSet>;
    abstract addTileSet(tileSet: TileSet): void;
    abstract removeTileSet(index: number): void;

    abstract getValidSets(tiles: Array<Tile>): Array<Array<TileSet>>;
}

export class Gameboard implements GameboardInterface{
    public static readonly BLACK_TILES: Tile[] = [
        new Tile(1, Color.black),
        new Tile(2, Color.black),
        new Tile(3, Color.black),
        new Tile(4, Color.black),
        new Tile(5, Color.black),
        new Tile(6, Color.black),
        new Tile(7, Color.black),
        new Tile(8, Color.black),
        new Tile(9, Color.black),
        new Tile(10, Color.black),
        new Tile(11, Color.black),
        new Tile(12, Color.black),
        new Tile(13, Color.black),
    ];

    public static readonly BLUE_TILES: Tile[] = [
        new Tile(1, Color.blue),
        new Tile(2, Color.blue),
        new Tile(3, Color.blue),
        new Tile(4, Color.blue),
        new Tile(5, Color.blue),
        new Tile(6, Color.blue),
        new Tile(7, Color.blue),
        new Tile(8, Color.blue),
        new Tile(9, Color.blue),
        new Tile(10, Color.blue),
        new Tile(11, Color.blue),
        new Tile(12, Color.blue),
        new Tile(13, Color.blue),
    ];

    public static readonly RED_TILES: Tile[] = [
        new Tile(1, Color.red),
        new Tile(2, Color.red),
        new Tile(3, Color.red),
        new Tile(4, Color.red),
        new Tile(5, Color.red),
        new Tile(6, Color.red),
        new Tile(7, Color.red),
        new Tile(8, Color.red),
        new Tile(9, Color.red),
        new Tile(10, Color.red),
        new Tile(11, Color.red),
        new Tile(12, Color.red),
        new Tile(13, Color.red),
    ];

    public static readonly GREEN_TILES: Tile[] = [
        new Tile(1, Color.green),
        new Tile(2, Color.green),
        new Tile(3, Color.green),
        new Tile(4, Color.green),
        new Tile(5, Color.green),
        new Tile(6, Color.green),
        new Tile(7, Color.green),
        new Tile(8, Color.green),
        new Tile(9, Color.green),
        new Tile(10, Color.green),
        new Tile(11, Color.green),
        new Tile(12, Color.green),
        new Tile(13, Color.green),
    ];

    public static readonly WILD_TILES: Tile[] = [
        new Tile(-1, null),
        new Tile(-2, null),
    ];


    public static readonly ALL_TILES: Tile[] = [
        ...Gameboard.WILD_TILES,
        ...Gameboard.BLACK_TILES,
        ...Gameboard.BLUE_TILES,
        ...Gameboard.RED_TILES,
        ...Gameboard.GREEN_TILES,
    ];

    private _sets: Array<TileSet>;

    constructor(){
        this._sets = [];
    };

    public getTileSets(): TileSet[] {
        return this._sets;   
    }

    public addTileSet(tileSet: TileSet): void {
        this._sets.push(tileSet);
    }
    public removeTileSet(index: number): void {
        if(index < 0 || this._sets.length < index) return;
        
        this._sets.splice(index, 1);
    }

    public getValidSets(tiles: Array<Tile>): Array<Array<TileSet>>{
        let validGameboards: Array<Array<TileSet>> = [];
        this.getValidSetsHelper(tiles, [], validGameboards, new Set(), tiles.length / TileSet.MIN_SET_LENGTH);
        return validGameboards;
    }

    private getValidSetsHelper(tiles: Array<Tile>, workingSet: Array<TileSet>, validSets: Array<Array<TileSet>>, seen: Set<string>, maxSets: number): void{
        // if(validSets.length > 0) return;
        if(workingSet.length > maxSets) return;


        let tileKeyBuilder = new StringBuilder();
        // for(let tile of tiles){
        //     tileKeyBuilder.Append(tile.color.toString());
        //     tileKeyBuilder.Append(tile.value.toString());
        //     tileKeyBuilder.Append(',');
        // }
        tileKeyBuilder.Append(tiles.length.toString());

        let workingSetKeyBuilder = new StringBuilder();

        for(let set of workingSet){
            workingSetKeyBuilder.Append((set instanceof GroupTileSet ? 'G' : 'R'));
            for(let tile of set.tiles()){
                workingSetKeyBuilder.Append(tile.color.toString());
                workingSetKeyBuilder.Append(tile.value.toString());
                workingSetKeyBuilder.Append(',');
            }
            workingSetKeyBuilder.Append('&');
        }


        let key = `${tileKeyBuilder.ToString()}:${workingSetKeyBuilder.ToString()}`;

        if(seen.has(key)) return;
        seen.add(key);

        if(tiles.length == 0){
            let isValid = workingSet
                .map(wSet => wSet.isValid())
                .reduce((wSet1, wSet2) => wSet1 && wSet2, true);

            let isNew: boolean = ! validSets
                .map(set => Gameboard.areEqual(set, workingSet))
                .reduce((a,b) => a || b, false);

            if(isValid && isNew){
                validSets.push(this.cloneTileSetArray(workingSet));
            }

            return;
        }

        let tile: Tile = tiles.pop();

        for(let i = 0; i < workingSet.length; i++){

            if(workingSet[i].canAdd(tile)){

                workingSet[i].add(tile);
                this.getValidSetsHelper(tiles, workingSet, validSets, seen, maxSets);
                workingSet[i].remove(tile);
            }

        }

        let newGroupSet: TileSet =  new GroupTileSet(tile.value);
        let newRunSet: TileSet =  new RunTileSet(tile.color);

        newGroupSet.add(tile);
        newRunSet.add(tile);

        workingSet.push(newGroupSet);
        this.getValidSetsHelper(tiles, workingSet, validSets, seen, maxSets);
        workingSet.pop();

        workingSet.push(newRunSet);
        this.getValidSetsHelper(tiles, workingSet, validSets, seen, maxSets);
        workingSet.pop();

        tiles.push(tile);
    }


    public static areEqual(tiles1: Array<TileSet>, tiles2: Array<TileSet>) : boolean{
        if(tiles1.length != tiles2.length) return false;

        let contains1: boolean = tiles1.map(set => this.arrayContains(tiles2, set)).reduce((a,b) => a && b, true);
        let contains2: boolean = tiles2.map(set => this.arrayContains(tiles1, set)).reduce((a,b) => a && b, true);

        return contains1 && contains2;
    }

    public static arrayContains(tileSets: Array<TileSet>, tileSet: TileSet) : boolean{
        return tileSets.map(set => set.equals(tileSet)).reduce((a,b) => a || b, false);
    }

    private cloneTileSetArray(arr: Array<TileSet>): Array<TileSet>{
        let copy = [];
        arr.forEach(tileSet => copy.push(tileSet.clone()));

        return copy;
    }

    private isSetsEqual = (a, b) => a.size === b.size && [...a].every(value => b.has(value));

    private printArrayOfTileSets(array: Array<TileSet>){
        console.log('Array Start:');
        array.forEach(t => console.log(t.toString() + ' : ' + t.isValid()));
        console.log('Array End');
    }
}

export class Hand{
    public tiles: TileSet

    constructor(){
        this.tiles = new AnyTileSet();
    }
}