import { Color, Tile, TileSet, GroupTileSet, RunTileSet, Gameboard } from './lib/models';

describe('Tile', () => {
  it('test equals', testEquals);
});

describe('TileSet', () => {
  it('test equals with run', runTileSetEqualsTest);
  it('test equals with group', groupTileSetEqualsTest);

});

describe('GroupTileSet', () => {
  it('test isValid', testGroupTileSetIsValid);
});

describe('RunTileSet', () => {
  it('test isValid', testRunTileSetIsValidStandard);
  it('test isValid', testRunTileSetIsValidWild);
});


describe('Gameboard', () => {
  it('arrayContains', arrayContainsTest);
  it('areEqual', areEqualTest);

  it('getValidSets run', getValidSetsRunTest);
  it('getValidSets group', getValidSetsGroupTest);
  it('getValidSets group and run', getValidSetsGroupAndRunTest);
});

  function testEquals(){
    let blackOne: Tile = new Tile(1, Color.black);
    let blackOneCopy: Tile = new Tile(1, Color.black);
    let blackTwo: Tile = new Tile(2, Color.black);
    let redOne:Tile = new Tile(1, Color.red);

    expect(blackOne.equals(blackOneCopy)).toBeTruthy();
    expect(blackOne.equals(blackOne)).toBeTruthy();

    expect(blackOne.equals(blackTwo)).toBeFalsy();
    expect(blackOne.equals(redOne)).toBeFalsy();
  }

  function runTileSetEqualsTest(){
    let run1: TileSet = new RunTileSet(Color.red);
    run1.addAll([new Tile(1, Color.red), new Tile(9, Color.red), new Tile(5, Color.red)]);

    let run2: TileSet = new RunTileSet(Color.red);
    run2.addAll([new Tile(5, Color.red), new Tile(1, Color.red), new Tile(9, Color.red)]);

    expect(run1.equals(run1)).toBeTruthy();
    expect(run2.equals(run2)).toBeTruthy();

    expect(run1.equals(run2)).toBeTruthy();
    expect(run2.equals(run1)).toBeTruthy();

    let run3: TileSet = new RunTileSet(Color.red);
    run3.addAll([new Tile(5, Color.red), new Tile(1, Color.red)]);

    expect(run3.equals(run1)).toBeFalsy();

    run3.addAll([new Tile(9, Color.red), new Tile(3, Color.red)]);

    expect(run3.equals(run1)).toBeFalsy();
  }

  function groupTileSetEqualsTest(){
    let group1: GroupTileSet = new GroupTileSet(1);
    group1.addAll([new Tile(1, Color.red), new Tile(1, Color.black), new Tile(1, Color.green)]);

    let group2: GroupTileSet = new GroupTileSet(1);
    group2.addAll([new Tile(1, Color.green), new Tile(1, Color.red), new Tile(1, Color.black)]);

    expect(group1.equals(group1)).toBeTruthy();
    expect(group2.equals(group2)).toBeTruthy();

    expect(group1.equals(group2)).toBeTruthy();
  }

  function testGroupTileSetIsValid(){
    let groupTileSet: TileSet = new GroupTileSet(2);
    groupTileSet.addAll([
      new Tile(2, Color.black),
      new Tile(2, Color.red),
    ]);

    expect(groupTileSet.isValid()).toBeFalsy();
    
    groupTileSet.add(new Tile(2, Color.green));

    expect(groupTileSet.isValid()).toBeTruthy();
  }

  function testRunTileSetIsValidStandard(){
    let runTileSet: TileSet = new RunTileSet(Color.red);
    runTileSet.addAll(
      [
        new Tile(1,Color.red),
        new Tile(2, Color.red),
      ]
    );

    expect(runTileSet.isValid()).toBeFalsy();

    runTileSet.add(new Tile(3, Color.red));

    expect(runTileSet.isValid()).toBeTruthy();


    runTileSet = new RunTileSet(Color.black);
    runTileSet.addAll(
      [
        new Tile(1,Color.black),
        new Tile(2, Color.black),
        new Tile(4, Color.black),
      ]
    );

    expect(runTileSet.isValid()).toBeFalsy();

  }

  function testRunTileSetIsValidWild(){
    let runTileSet: TileSet = new RunTileSet(Color.red);
    runTileSet.addAll(
      [
        new Tile(1,Color.red),
        new Tile(4, Color.red),
      ]
    );

    expect(runTileSet.isValid()).toBeFalsy();

    runTileSet.add(new Tile(null, null));
    expect(runTileSet.isValid()).toBeFalsy();

    runTileSet.add(new Tile(null, null));
    expect(runTileSet.isValid()).toBeTruthy();
  }

  function arrayContainsTest(){
    let run: TileSet = new RunTileSet(Color.black);
    run.addAll([new Tile(1, Color.black), new Tile(1, Color.red)]);

    let run2: TileSet = new RunTileSet(Color.black);
    run.addAll([new Tile(1, Color.black)]);

    expect(Gameboard.arrayContains([run], run)).toBeTruthy();
    expect(Gameboard.arrayContains([run, run2], run2)).toBeTruthy();
    expect(Gameboard.arrayContains([run], run2)).toBeFalsy();
  }

  function areEqualTest(){
    let run: TileSet = new RunTileSet(Color.black);
    run.addAll([new Tile(1, Color.black), new Tile(1, Color.red)]);

    let run2: TileSet = new RunTileSet(Color.black);
    run.addAll([new Tile(1, Color.black)]);

    expect(Gameboard.areEqual([run], [run.clone()])).toBeTruthy();
    expect(Gameboard.areEqual([run, run2], [run.clone(), run2.clone()])).toBeTruthy();

    expect(Gameboard.areEqual([run, run2], [run.clone()])).toBeFalsy();
    expect(Gameboard.areEqual([run], [run.clone(), run2.clone()])).toBeFalsy();
  }

  function getValidSetsRunTest(){
    let gameboard: Gameboard = new Gameboard();
    let tiles: Array<Tile> = [
      new Tile(1, Color.black),
      new Tile(2, Color.black),
      new Tile(3, Color.black),
    ];
    let validSet = new RunTileSet(Color.black);
    validSet.addAll(tiles);

    let vals = gameboard.getValidSets(tiles);

    expect(vals.length).toBe(1);
    expect(Gameboard.areEqual(vals[0], [validSet])).toBeTruthy();
  }

  function getValidSetsGroupTest(){
    let gameboard: Gameboard = new Gameboard();
    let tiles: Array<Tile> = [
      new Tile(1, Color.black),
      new Tile(1, Color.red),
      new Tile(1, Color.green),
    ];
    let validSet = new GroupTileSet(1);
    validSet.addAll(tiles);

    let vals = gameboard.getValidSets(tiles);

    expect(vals.length).toBe(1);
    expect(vals[0][0].equals(validSet)).toBeTruthy();
    expect(Gameboard.areEqual(vals[0], [validSet])).toBeTruthy();
  }

  function getValidSetsGroupAndRunTest(){
    let gameboard: Gameboard = new Gameboard();

    let blackTiles: Array<Tile> = [
      new Tile(1, Color.black),
      new Tile(2, Color.black),
      new Tile(3, Color.black),
    ];

    let redTiles: Array<Tile> = [
      new Tile(1, Color.red),
      new Tile(2, Color.red),
      new Tile(3, Color.red),
    ];

    let blueTiles: Array<Tile> = [
      new Tile(1, Color.blue),
      new Tile(2, Color.blue),
      new Tile(3, Color.blue),
    ];

    let tiles: Array<Tile> = [
      ...blackTiles,
      ...redTiles,
      ...blueTiles
    ];

    let blackRun = new RunTileSet(Color.black);
    let redRun = new RunTileSet(Color.red);
    let blueRun = new RunTileSet(Color.blue);
    
    let oneGroup = new GroupTileSet(1);
    let twoGroup = new GroupTileSet(2);
    let threeGroup = new GroupTileSet(3);

    blackRun.addAll(blackTiles);
    redRun.addAll(redTiles);
    blueRun.addAll(blueTiles);
   
    oneGroup.addAll([blackTiles[0], redTiles[0], blueTiles[0]]);
    twoGroup.addAll([blackTiles[1], redTiles[1], blueTiles[1]]);
    threeGroup.addAll([blackTiles[2], redTiles[2], blueTiles[2]]);

    let vals = gameboard.getValidSets(tiles);

    let validRunBoard = [
      blackRun,
      redRun,
      blueRun
    ];

    let validGroupBoard = [
      oneGroup,
      twoGroup,
      threeGroup
    ];

    expect(vals.length).toBe(2);
    expect(Gameboard.areEqual(vals[0], vals[1])).toBeFalsy();
    expect(Gameboard.areEqual(vals[0], validRunBoard) || Gameboard.areEqual(vals[0], validGroupBoard)).toBeTruthy();
    expect(Gameboard.areEqual(vals[1], validRunBoard) || Gameboard.areEqual(vals[1], validGroupBoard)).toBeTruthy();
  }