import { Component, OnInit, Inject } from '@angular/core';
import { TileSelectionComponent } from '../tile-selection/tile-selection.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TileSet } from '../lib/models';

@Component({
  selector: 'app-solutions',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.scss']
})
export class SolutionsComponent implements OnInit {
  currentSolutionIndex: number;

  constructor(
    public dialogRef: MatDialogRef<TileSelectionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TileSet[][]) {
      this.currentSolutionIndex = 0;
    
    }
    
    ngOnInit() {
  
    }

    getSolution(){
      return this.data[this.currentSolutionIndex];
    }

}
