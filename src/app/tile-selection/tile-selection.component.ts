import { Component, OnInit, Inject, Input } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { Color, Tile, TileSet, Gameboard } from '../lib/models';

@Component({
  selector: 'app-tile-selection',
  templateUrl: './tile-selection.component.html',
  styleUrls: ['./tile-selection.component.scss']
})
export class TileSelectionComponent implements OnInit {
  readonly colors = [Color.black, Color.blue, Color.green, Color.red];
  readonly numbers = [1,2,3,4,5,6,7,8,9,10,11,12,13];

  validTiles: Tile[];

  constructor(
    public dialogRef: MatDialogRef<TileSelectionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TileSet) {}
  
  
  ngOnInit() {
    this.validTiles = Gameboard.ALL_TILES.filter(t => this.data.canAdd(t) || this.data.containsTile(t));
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onDoneClicked(){
    this.dialogRef.close();
  }

  getTileClass(tile: Tile){
    return {
      'op': !this.data.containsTile(tile)
    }
  }

  onTileClicked(tile: Tile){
    if(this.data.containsTile(tile)){
      this.data.remove(tile);
    }else{
      this.data.add(tile);
    }
  }

  getColorText(color: Color){
    switch(color){
      case Color.black:
        return 'Black';
      case Color.red:
        return 'Red';
      case Color.green:
        return 'Green';
      case Color.blue:
        return 'Blue';
    }
  }

}
