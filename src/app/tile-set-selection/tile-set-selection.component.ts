import { Component, OnInit, Inject } from '@angular/core';
import { TileSet, RunTileSet, Color, GroupTileSet, Tile } from '../lib/models';
import { MAT_DIALOG_DATA, MatDialogRef, MatSelectChange } from '@angular/material';

export interface TileSetSelectionData {
  tileSet : TileSet;
}

@Component({
  selector: 'app-tile-set-selection',
  templateUrl: './tile-set-selection.component.html',
  styleUrls: ['./tile-set-selection.component.scss']
})
export class TileSetSelectionComponent implements OnInit {
  setType: string = 'run';
  selectedColor: string = 'black';
  colors = [Color.black, Color.black, Color.red, Color.green];


  constructor(
    public dialogRef: MatDialogRef<TileSetSelectionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TileSetSelectionData) {}

  ngOnInit() {
    this.data.tileSet = new RunTileSet(Color.black);
    this.data.tileSet.add(new Tile(1, Color.black));
    this.data.tileSet.add(new Tile(2, Color.black));
    this.data.tileSet.add(new Tile(3, Color.black));  
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onAddClicked(){
    this.dialogRef.close(this.data.tileSet);
  }

  onChangeSetType(newSelection: MatSelectChange){
    switch(newSelection.value){
      case 'group':
        this.onGroup();
        break;
      case 'run':
        this.onRun();
    }
  }

  onChangeColor(newSelection: MatSelectChange){
    this.selectedColor = newSelection.value;
    this.onRun();
  }

  onGroup(){
    this.data.tileSet = new GroupTileSet(1);
    this.data.tileSet.add(new Tile(1, Color.black));
    this.data.tileSet.add(new Tile(1, Color.red));
    this.data.tileSet.add(new Tile(1, Color.green));
  }

  onRun(){

    let color: Color = null;
    switch(this.selectedColor){
      case 'black':
        color = Color.black;
        break;
      case 'blue':
        color = Color.blue;
        break;
      case 'red':
        color = Color.red;
        break;
      case 'green':
        color = Color.green;
        break;
    }

    this.data.tileSet = new RunTileSet(color);
    this.data.tileSet.add(new Tile(1, color));
    this.data.tileSet.add(new Tile(2, color));
    this.data.tileSet.add(new Tile(3, color));
  }

}
