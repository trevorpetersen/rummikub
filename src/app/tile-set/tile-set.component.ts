import { Component, OnInit, Input } from '@angular/core';
import { TileSet, Tile } from '../lib/models';

@Component({
  selector: 'app-tile-set',
  templateUrl: './tile-set.component.html',
  styleUrls: ['./tile-set.component.scss']
})
export class TileSetComponent implements OnInit {
  @Input('tileList')
  tileList: Tile[];

  constructor() {
  }

  ngOnInit() {
  }

}
