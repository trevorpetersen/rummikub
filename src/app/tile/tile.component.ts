import { Component, OnInit, Input } from '@angular/core';
import { Tile, Color } from '../lib/models';

@Component({
  selector: 'app-tile',
  templateUrl: './tile.component.html',
  styleUrls: ['./tile.component.scss']
})
export class TileComponent implements OnInit {

  @Input('tile')
  tile: Tile;

  constructor() { }

  ngOnInit() {
  }

  getClass(){
    return {
      'black': this.tile.color == Color.black,
      'blue': this.tile.color == Color.blue,
      'green': this.tile.color == Color.green,
      'red': this.tile.color == Color.red,
    }
  }

}
